package com.kvng_willy.applaunch.util

import android.content.Context
import android.view.View
import android.widget.*
import com.google.android.material.snackbar.Snackbar

fun Context.toast(message:String){
    Toast.makeText(this,message,Toast.LENGTH_LONG).show()
}

fun View.snackbar(message: String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
        snackbar.setAction("Ok") {
            snackbar.dismiss()
        }
    }.show()
}

fun ScrollView.hide(){
    visibility = View.GONE
}

fun ScrollView.show(){
    visibility = View.VISIBLE
}

fun ListView.hide(){
    visibility = View.GONE
}

fun ListView.show(){
    visibility = View.VISIBLE
}