package com.kvng_willy.applaunch.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.kvng_willy.applaunch.ui.PopularItems
import com.kvng_willy.applaunch.ui.ShoppingItems

class SectionsPageAdapter(fa:FragmentActivity):FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = TRANSACTION_SCREENS_NUMBER

    override fun createFragment(position: Int): Fragment = when (position) {
        POPULARITEMSVIEW -> PopularItems()
        SHOPPINGITEMSVIEW -> ShoppingItems()
        else -> throw IllegalStateException("Invalid adapter position")
    }

    companion object {
        internal const val TRANSACTION_SCREENS_NUMBER = 2

        internal const val POPULARITEMSVIEW = 0
        internal const val SHOPPINGITEMSVIEW = 1
    }
}