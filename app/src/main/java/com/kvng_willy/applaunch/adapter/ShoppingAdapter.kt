package com.kvng_willy.applaunch.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.kvng_willy.applaunch.R
import com.kvng_willy.applaunch.data.network.ShoppingList

class ShoppingAdapter(private val context: Context, private val shoppingList:List<ShoppingList>):BaseAdapter() {
    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        return shoppingList.size
    }

    override fun getItem(position: Int): Any {
        return shoppingList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun isEnabled(position: Int): Boolean {
        val list = getItem(position) as ShoppingList
        if(list.available.equals("no",true)){
            return false
        }
        return super.isEnabled(position)
    }

    override fun areAllItemsEnabled(): Boolean {
        return super.areAllItemsEnabled()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.item_shop, parent, false)
        val list = getItem(position) as ShoppingList

        val name = rowView.findViewById(R.id.listName) as TextView
        val available = rowView.findViewById(R.id.available) as TextView
        val rootView = rowView.findViewById(R.id.rootView) as LinearLayout
        name.text = list.name
        available.text = list.available

        if(list.available.equals("No",true)){
            rootView.isEnabled = false
            rootView.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.light_grey,context.resources.newTheme()))
            available.setTextColor(ResourcesCompat.getColor(context.resources, android.R.color.holo_red_light,context.resources.newTheme()))
        }
        return rowView
    }
}