package com.kvng_willy.applaunch.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kvng_willy.applaunch.data.db.AppDatabase
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket
import com.kvng_willy.applaunch.data.network.MyApi
import retrofit2.Callback
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class DataRepository( private val api: MyApi,
private val db:AppDatabase){
    fun getShoppingList(): LiveData<String> {
        val jsonResponse = MutableLiveData<String>()
        api.getShoppingList().enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                jsonResponse.value = "An error occurred"
            }
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful) {
                    jsonResponse.value = response.body()?.string()
                }else {
                    jsonResponse.value = "An error occurred"
                }
            }

        })
        return jsonResponse
    }

    suspend fun saveShopping(shoppingBasket: ShoppingBasket) = db.getShoppingDao().upsert(shoppingBasket)

    fun getShopping() = db.getShoppingDao().getShoppingItem()

    suspend fun deleteShopping(uid:Int) = db.getShoppingDao().delete(uid)
}