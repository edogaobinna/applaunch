package com.kvng_willy.applaunch.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager


class PreferenceProvider(
    context: Context
) {

    private val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)


    fun savePreference(key:String, savedAt: String?) {
        preference.edit().putString(
            key,
            savedAt
        ).apply()
    }
    fun deletePreference(key:String){
        preference.edit().remove(key).apply()
    }
    fun checkPreference(key:String):Boolean{
        return preference.contains(key)
    }

    fun getLastSavedAt(key:String): String? {
        return preference.getString(key, "1")
    }

    fun deleteAll(){
        preference.edit().clear().apply()
    }

}