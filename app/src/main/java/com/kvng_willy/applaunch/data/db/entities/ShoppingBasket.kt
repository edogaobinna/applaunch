package com.kvng_willy.applaunch.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Date

@Entity
data class ShoppingBasket(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    var name: String,
    var quantity: String,
    var dateCreated: Date? = null
)