package com.kvng_willy.applaunch.data.network

data class ShoppingList (
    val name: String?,
    val available: String?
)