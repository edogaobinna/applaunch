package com.kvng_willy.applaunch.ui

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.applaunch.R
import com.kvng_willy.applaunch.adapter.Shopping2Adapter
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket
import com.kvng_willy.applaunch.data.repositories.DataRepository
import com.kvng_willy.applaunch.databinding.ShoppingItemsFragmentBinding
import com.kvng_willy.applaunch.util.hide
import com.kvng_willy.applaunch.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class ShoppingItems : Fragment(),KodeinAware,Listener {

    companion object {
        fun newInstance() = ShoppingItems()
    }

    private lateinit var binding: ShoppingItemsFragmentBinding
    override val kodein by kodein()

    private val factory: ShoppingItemsModelFactory by instance()
    private val repository: DataRepository by instance()
    private lateinit var viewmodel: ShoppingItemsViewModel
    private var listShopping:ArrayList<ShoppingBasket> = ArrayList()
    private lateinit var shoppingAdapter: Shopping2Adapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.shopping_items_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewmodel =
            ViewModelProvider(this, factory).get(ShoppingItemsViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.listener = this
        shoppingAdapter = Shopping2Adapter(requireContext(),listShopping,repository)

        viewmodel.shoppingList.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                listShopping.clear()
                for(e in it.indices){
                    listShopping.add(it[e])
                }
                binding.rootView.hide()
                binding.listView.show()
                binding.listView.adapter = shoppingAdapter
                shoppingAdapter.notifyDataSetChanged()
            }else{
                shoppingAdapter.notifyDataSetChanged()
                binding.listView.hide()
                binding.rootView.show()
            }

        })

        binding.searchView.setOnQueryTextListener(object:SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewmodel.shoppingList.observe(viewLifecycleOwner, Observer {int->
                    if(int.isNotEmpty()){
                        listShopping.clear()
                        for(e in int.indices){
                            listShopping.add(int[e])
                        }
                        shoppingAdapter.notifyDataSetChanged()

                    }
                })
                shoppingAdapter.getFilter()?.filter(newText)
                return false
            }

        })
    }

    override fun onTaskReturn(message: LiveData<String>?) {

    }

}