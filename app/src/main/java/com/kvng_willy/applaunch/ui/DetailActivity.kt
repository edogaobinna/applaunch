package com.kvng_willy.applaunch.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.applaunch.R
import com.kvng_willy.applaunch.data.network.ShoppingList
import com.kvng_willy.applaunch.databinding.ActivityDetailBinding
import com.kvng_willy.applaunch.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class DetailActivity : AppCompatActivity(),KodeinAware,Listener {
    override val kodein by kodein()
    private val factory: GenericModelFactory by instance()
    private lateinit var viewmodel: GenericViewModel


    companion object {
        const val EXTRA_NAME = "name"

        fun newIntent(context: Context, shopping: ShoppingList): Intent {
            val detailIntent = Intent(context, DetailActivity::class.java)

            detailIntent.putExtra(EXTRA_NAME, shopping.name)
            return detailIntent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val binding: ActivityDetailBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_detail
        )
        viewmodel = ViewModelProvider(this, factory).get(GenericViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.listener = this
        viewmodel.name = intent.extras!!.getString(EXTRA_NAME)!!
        binding.lifecycleOwner = this

        binding.listName.text = intent.extras!!.getString(EXTRA_NAME)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onTaskReturn(message: LiveData<String>?) {
        toast("Added to cart")
    }
}