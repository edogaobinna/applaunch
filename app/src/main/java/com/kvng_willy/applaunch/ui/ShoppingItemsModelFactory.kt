package com.kvng_willy.applaunch.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.applaunch.data.preferences.PreferenceProvider
import com.kvng_willy.applaunch.data.repositories.DataRepository
import java.io.UncheckedIOException

@Suppress("UNCHECKED_CAST")
class ShoppingItemsModelFactory(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ShoppingItemsViewModel(repository,prefs) as T
        }
}