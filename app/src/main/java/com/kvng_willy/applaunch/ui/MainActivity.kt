package com.kvng_willy.applaunch.ui

import android.content.Intent
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.kvng_willy.applaunch.R
import com.kvng_willy.applaunch.adapter.SectionsPageAdapter

class MainActivity : FragmentActivity() {

    val tabTitles = arrayOf<String>("Popular Items", "Shopping Items")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val viewPager: ViewPager2 = findViewById(R.id.view_pager)
        val sectionsPageAdapter = SectionsPageAdapter(this)
        val fab: FloatingActionButton = findViewById(R.id.fab)
        viewPager.adapter = sectionsPageAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        TabLayoutMediator(tabs, viewPager){ tab, position->
            when (position) {
                0 -> {
                    tab.text = tabTitles[position]
                    tab.icon = ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_baseline_star,
                        resources.newTheme()
                    )
                }
                1 -> {
                    tab.text = tabTitles[position]
                    tab.icon = ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_shopping_bag,
                        resources.newTheme()
                    )
                }
            }
        }.attach()

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> fab.hide()
                    1 -> fab.show()
                    else -> fab.hide()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        fab.setOnClickListener { view ->
            Intent(this, NewItemActivity::class.java).also {
                startActivity(it)
            }
        }
    }
}