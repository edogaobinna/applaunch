package com.kvng_willy.applaunch.ui

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket
import com.kvng_willy.applaunch.data.preferences.PreferenceProvider
import com.kvng_willy.applaunch.data.repositories.DataRepository
import com.kvng_willy.applaunch.util.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.sql.Date
import java.util.*


class GenericViewModel(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
) : ViewModel() {

    var listener: Listener? = null
    private val responses = MutableLiveData<String>()

    var quantity:String = "1"
    var name:String = ""

    fun onAddToCart(view: View) {
        val shoppingBasket = ShoppingBasket(null,name,quantity,Date(Calendar.getInstance().timeInMillis))
        Coroutines.io {
            repository.saveShopping(shoppingBasket)
        }
        listener?.onTaskReturn(responses)
    }

    fun onAddNewItem(view: View) {
        val shoppingBasket = ShoppingBasket(null,name,quantity,Date(Calendar.getInstance().timeInMillis))
        Coroutines.io {
            repository.saveShopping(shoppingBasket)
        }
        listener?.onTaskReturn(responses)
    }
}